import numpy as np
import cv2
import matplotlib.pyplot as plt

# cap = cv2.VideoCapture(0)

# while cap.isOpened():
#     ret, frame = cap.read()

#     hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
#     lower_red = np.array([110,50,50])
#     upper_red = np.array([255,255,255])
#     mask = cv2.inRange(hsv, lower_red, upper_red)

#     res = cv2.bitwise_and(frame, frame, mask=mask)
#     cv2.imshow("frame", frame)
#     cv2.imshow("mask", mask)
#     cv2.imshow("res", res)

#     if cv2.waitKey(1) & 0xFF == ord('q'):
#         break

# cv2.destroyAllWindows()

# ______________________________________________________________________

# # Image filtering for sharpening with manually adding image kernel
# kernel = np.array([[0, -1, 0],
#                    [-1, 5, -1],
#                    [0, -1, 0],])

# cap = cv2.VideoCapture(0)

# while cap.isOpened():
#     ret, frame  = cap.read()
#     blur_frame = cv2.filter2D(frame, -1, kernel)
#     cv2.imshow("frame", blur_frame)
#     if cv2.waitKey(1) & 0xFF == ord('q'):
#         break

# cap.release()
# cv2.destroyAllWindows()

# ______________________________________________________________________

# # Edge Detection with Canny
# cap = cv2.VideoCapture(0)

# while cap.isOpened():
#     ret, frame = cap.read()

#     # Convert frame to grayscale
#     gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
#     cv2.imshow("Grayscale", gray)

#     # Apply Gaussian blur
#     blurred = cv2.GaussianBlur(gray, (5, 5), 0)
#     cv2.imshow("Blurred", blurred)

#     # Perform Canny edge detection
#     edges = cv2.Canny(blurred, 50, 150)
#     cv2.imshow("Edges", edges)

#     # Perform double thresholding
#     lower_threshold = 50
#     upper_threshold = 150
#     thresholded = cv2.inRange(edges, lower_threshold, upper_threshold)

#     # Perform convolution using the manually defined kernel
#     convolved = cv2.filter2D(thresholded, -1, kernel)

#     cv2.imshow("Convolved", convolved)

#     if cv2.waitKey(1) & 0xFF == ord('q'):
#         break

# cap.release()
# cv2.destroyAllWindows()

# ______________________________________________________________________

import cv2
import numpy as np

cap = cv2.VideoCapture(0)

while cap.isOpened():
    ret, frame = cap.read()

    # Convert frame to grayscale
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    color_equalized = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    color_equalized[:, :, 0] = cv2.equalizeHist(color_equalized[:, :, 0])
    color_equalized[:, :, 1] = cv2.equalizeHist(color_equalized[:, :, 1])
    color_equalized[:, :, 2] = cv2.equalizeHist(color_equalized[:, :, 2])

    # Perform histogram equalization
    equalized = cv2.equalizeHist(gray)

    # Calculate histograms
    hist_original = cv2.calcHist([gray], [0], None, [256], [0, 256])
    hist_equalized = cv2.calcHist([equalized], [0], None, [256], [0, 256])
    hist_color_eq = cv2.calcHist
    # for color
    hist_r = cv2.calcHist([color_equalized], [0], None, [256], [0, 256])
    hist_g = cv2.calcHist([color_equalized], [1], None, [256], [0, 256])
    hist_b = cv2.calcHist([color_equalized], [2], None, [256], [0, 256])
    

    # Normalize histograms for display
    hist_original = cv2.normalize(hist_original, None, 0, 255, cv2.NORM_MINMAX, dtype=cv2.CV_8U)
    hist_equalized = cv2.normalize(hist_equalized, None, 0, 255, cv2.NORM_MINMAX, dtype=cv2.CV_8U)
    hist_r = cv2.normalize(hist_r, None, 0, 255, cv2.NORM_MINMAX)
    hist_g = cv2.normalize(hist_g, None, 0, 255, cv2.NORM_MINMAX)
    hist_b = cv2.normalize(hist_b, None, 0, 255, cv2.NORM_MINMAX)

    # Create blank images to display histograms
    hist_original_img = np.zeros((256, 256), dtype=np.uint8)
    hist_equalized_img = np.zeros((256, 256), dtype=np.uint8)
    hist_img_r = np.zeros((256, 256, 3), dtype=np.uint8)
    hist_img_g = np.zeros((256, 256, 3), dtype=np.uint8)
    hist_img_b = np.zeros((256, 256, 3), dtype=np.uint8)

    # Draw histograms on images
    for x, y in enumerate(hist_original):
        cv2.line(hist_original_img, (x, 255), (x, int(255 - y)), 255)
    for x, y in enumerate(hist_equalized):
        cv2.line(hist_equalized_img, (x, 255), (x, int(255 - y)), 255)
    for i in range(256):
        cv2.line(hist_img_r, (i, 255), (i, 255 - int(hist_r[i])), (0, 0, 255))
        cv2.line(hist_img_g, (i, 255), (i, 255 - int(hist_g[i])), (0, 255, 0))
        cv2.line(hist_img_b, (i, 255), (i, 255 - int(hist_b[i])), (255, 0, 0))


    # Display images
    cv2.imshow("Color Equalized", color_equalized)
    cv2.imshow("Original", gray)
    cv2.imshow("Equalized", equalized)
    cv2.imshow("Histogram 1", hist_original_img)
    cv2.imshow("Histogram Equalized", hist_equalized_img)
    cv2.imshow("Red Histogram", hist_img_r)
    cv2.imshow("Green Histogram", hist_img_g)
    cv2.imshow("Blue Histogram", hist_img_b)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()





