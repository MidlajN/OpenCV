import cv2
import torch


model = torch.hub.load('ultralytics/yolov5', 'yolov5m')
cap = cv2.VideoCapture(0)


def yolo_real_time():
    while True:
        ret, frame = cap.read()
        if ret:
            results = model(frame)
            print(results)
            for detection in results.xyxy[0]:

                x1, y1, x2, y2, conf, cls = detection.tolist()
                label = model.names[int(cls)]
                confidence = int(conf * 100)

                if confidence > 50:
                    text = f'{label} : {confidence}%'

                    cv2.rectangle(frame, (int(x1), int(y1)), (int(x2), int(y2)), (0, 255, 0), 2)
                    cv2.putText(frame, text,  (int(x1), int(y1) - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (0, 255, 0), 2)
 
            cv2.imshow('frame', frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
            
    cv2.destroyAllWindows()
    
yolo_real_time()