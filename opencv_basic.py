import cv2


# img1 = cv2.imread("/home/zoro/Downloads/photo_2023-05-07_20-32-22.jpg", 0)
# print(img1.shape)

# resized_img = cv2.resize(img1, (500, 300))
# cv2.imshow("img", resized_img)

# while True:
#     if cv2.waitKey(1) & 0xFF == 27 or cv2.getWindowProperty("img", cv2.WND_PROP_VISIBLE) < 1:
#         break

# --------------------------- Video Capture ------------------------------
drawing = False
mode = True
ix, iy = -1, -1
rectangles = []
circles = []

def draw(event, x, y, flags, param):
    global ix, iy, drawing, mode, rectangles

    frame_copy = frame.copy()

    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix, iy = x, y
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing:
            frame_copy = frame.copy()
            if mode:
                cv2.rectangle(frame_copy, (ix, iy), (x, y), (0, 255, 0), 2)
            else:
                cv2.circle(frame_copy, (x, y), 20, (0, 0, 255), -1)
        cv2.imshow("frame", frame_copy)

    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        
        if mode == True:
            rectangles.append((ix, iy, x, y))
            cv2.rectangle(frame_copy, (ix, iy), (x, y), (0, 255, 0), 2)
        else:
            circles.append((x, y))
            cv2.circle(frame_copy, (x, y), 20, (0, 0, 255), 2)
        cv2.imshow("frame", frame_copy)


cap = cv2.VideoCapture(0)
cv2.namedWindow("frame")

# Create a frame copy for drawing
ret, frame = cap.read()

cv2.setMouseCallback('frame', draw)

# Define the Codec and create VideoWriter Object
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('output.avi', fourcc, 10.0, (640, 480))


while cap.isOpened():
    ret, frame = cap.read()
    frame = cv2.resize(frame, (320, 240))

    if not ret:
        print("Cannot receive frame. Exiting ...")
        break

    for rect in rectangles:
        cv2.rectangle(frame, (rect[0], rect[1]), (rect[2], rect[3]), (0, 255, 0), 2)
    for circle in circles:
        cv2.circle(frame, (circle[0], circle[1]), 20, (0, 0, 255), -1)

    out.write(frame)
    cv2.imshow("frame", frame)

    k = cv2.waitKey(1)
    if k == ord('m'):
        mode = False
    elif k == 27 or cv2.getWindowProperty("frame", cv2.WND_PROP_VISIBLE) < 1:
        break


cap.release()   # release the capture
out.release()

cv2.destroyAllWindows()
