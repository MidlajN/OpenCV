import cv2
import numpy as np
import matplotlib.pyplot as plt


image = cv2.imread('sift.jpg')
# -------------------- Show Image and Test Image --------------------
training_img = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
training_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

test_img = cv2.pyrDown(training_img)
test_img = cv2.pyrDown(test_img)
num_rows, num_cols = test_img.shape[:2]

rotation_matrix = cv2.getRotationMatrix2D((num_cols / 2, num_rows / 2), 30, 1)
test_image = cv2.warpAffine(test_img, rotation_matrix, (num_cols, num_rows))

# test_gray = cv2.cvtColor(test_image, cv2.COLOR_BGR2GRAY)
test_gray = cv2.cvtColor(test_image, cv2.COLOR_BGR2RGB)

fx, plots = plt.subplots(1, 2, figsize=(20, 10))

plots[0].set_title('Training Image')
plots[0].imshow(training_img)

plots[1].set_title('Test Image')
plots[1].imshow(test_image)


# -------------------- SIFT Feature Extraction --------------------
sift = cv2.xfeatures2d.SIFT_create()

train_keypoints, train_descriptors = sift.detectAndCompute(training_gray, None)
test_keypoints, test_descriptors = sift.detectAndCompute(test_gray, None)


key_point_without_size = np.copy(training_img)
key_point_with_size = np.copy(training_img)

cv2.drawKeypoints(training_img, train_keypoints, key_point_without_size, color = (0, 255, 0))

cv2.drawKeypoints(training_img, train_keypoints, key_point_with_size, flags = cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

fx, plots = plt.subplots(1, 2, figsize=(20, 10))
plots[0].set_title('Training Keypoint with Size')
plots[0].imshow(key_point_with_size, cmap='gray')
plots[1].set_title('Training Keypoint without Size')
plots[1].imshow(key_point_without_size, cmap='gray')

print("Number of Keypoints Detected In The Training Image: ", len(train_keypoints))
# Print the number of keypoints detected in the query image
print("Number of Keypoints Detected In The Query Image: ", len(test_keypoints))


# -------------------- Matching Keypoints --------------------
bf = cv2.BFMatcher(cv2.NORM_L2, crossCheck=True)

matches = bf.match(train_descriptors, test_descriptors)

matches = sorted(matches, key = lambda x:x.distance)

result = cv2.drawMatches(training_img, train_keypoints, test_gray, test_keypoints, matches[:10], None, flags=2)

plt.rcParams['figure.figsize'] = [14.0, 7.0]
plt.title('Matches')
plt.imshow(result)
plt.show()