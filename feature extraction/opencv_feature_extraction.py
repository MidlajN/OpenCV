import cv2
import numpy as np

cap = cv2.VideoCapture(0)
laplacian_kernel = np.array([[-1, -1, -1],
                             [-1,  8, -1],
                             [-1, -1, -1]])

while cap.isOpened():
    ret, frame = cap.read()
    # edge = cv2.filter2D(frame, -1, laplacian_kernel)
    # cv2.imshow("frame", edge)

    # or we can use canny isntead of applying kernel directly
    # edge_canny = cv2.Canny(frame, 50, 150)
    # cv2.imshow("edge_canny", edge_canny)

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    gray_blurred = cv2.blur(gray, (9, 9), 0)

    detected_circle = cv2.HoughCircles(gray_blurred, cv2.HOUGH_GRADIENT, 1, 20, param1=80, param2=40, minRadius=8, maxRadius=100)
    
    if detected_circle is not None:
        detected_circle = np.uint16(np.around(detected_circle))
        for pt in detected_circle[0, :]:
            a, b, r = pt[0], pt[1], pt[2]
            cv2.circle(frame, (a, b), r, (0, 255, 0), 2)
            cv2.circle(frame, (a, b), 1, (0, 0, 255), 3)
            cv2.imshow("frame", frame)

    # cv2.imshow("frame", frame)


    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cv2.destroyAllWindows()



# while cap.isOpened():
#     ret, frame = cap.read()
#     z = np.float32(frame)
#     criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, .001)
#     k = 2
#     ret, label, center = cv2.kmeans(z, k, None, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)
#     center = np.uint8(center)
#     res = center[label.flatten()]
#     res2 = res.reshape((frame.shape))
#     cv2.imshow("frame", res2)

#     if cv2.waitKey(1) & 0xFF == ord('q'):
#         break