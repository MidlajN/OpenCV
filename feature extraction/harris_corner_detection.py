import cv2
import numpy as np

cap = cv2.VideoCapture(0)

while True:
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    gray = np.float32(gray)
    dst = cv2.cornerHarris(gray, 2, 3, 0.09)
    # dst = cv2.dilate(dst, None)


    # cornerSupPix() function which further refines the corners detected with sub pixel accuracy
    ret, dst = cv2.threshold(dst, 0.02 * dst.max(), 255, 0)

    dst = np.uint8(dst) 

    ret, label, stats, centers = cv2.connectedComponentsWithStats(dst)

    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 0.0001)
    corners = cv2.cornerSubPix(gray, np.float32(centers), (5, 5), (-1, -1), criteria)

    res = np.hstack((centers, corners))
    res = res.astype(np.intp)
    height, width = frame.shape[:2]

    for corner in res:
        x, y, refined_x, refined_y = corner
        if 0 <= refined_x < width and 0 <= refined_y < height:
            frame[refined_y, refined_x] = [0, 255, 0]
            frame[y, x] = [0, 0, 255]

    # frame[dst > 0.01 * dst.max()] = [0, 0, 255]
    cv2.imshow("frame", frame)


    cv2.imshow("frame", frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cap.release()
cv2.destroyAllWindows()