import cv2

video = cv2.VideoCapture(0)

class PixelDisplacement:
    def __init__(self):
        # tracker type and initialization
        self.tracker = cv2.TrackerMIL_create()
        ok, self.frame = video.read()
        if ok:
            self.bbox = cv2.selectROI(self.frame, False)
        self.first_position = self.bbox
        # initialize tracker with first frame
        self.ok = self.tracker.init(self.frame, self.bbox)
        self.displacement = None

    def tracking_object(self):
        while True:
            ret, frame = video.read()
            if not ret :
                break

            key = cv2.waitKey(1) & 0xFF
            if key == ord('s'):
                self.bbox = cv2.selectROI(frame, False)
                self.first_position = self.bbox
                self.ok = self.tracker.init(frame, self.bbox)
            
            ok, self.bbox = self.tracker.update(frame)
            if ok:
                x, y, w, h = self.bbox
                circle_center = x + w // 2, y + h // 2
            x1, y1, w1, h1 = self.first_position
            circle_center1 = x1 + w1 // 2, y1 + h1 // 2
            self.displacement = circle_center[0] - circle_center1[0]

            cv2.circle(frame, circle_center, 5, (0, 0, 255), -1)
            cv2.putText(frame, "Displacement : " + str(self.displacement), (100,20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50), 2)
            cv2.imshow("Tracking", frame)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
obj = PixelDisplacement()
obj.tracking_object()        