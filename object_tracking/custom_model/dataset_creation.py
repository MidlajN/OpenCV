import cv2

video = cv2.VideoCapture(0)
tracker = cv2.TrackerMIL_create()
ret, frame = video.read()
bbox = cv2.selectROI(frame, False)
ok = tracker.init(frame, bbox)

frame_count = 0

while True:
    ret, frame = video.read()
    if not ret:
        break
    ok, bbox = tracker.update(frame)
    if ok:
        x, y, w, h = bbox
        cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2, 1)
        
        
        # Save frame and annotation
        frame_count += 1
        height, width, _ = frame.shape
        norm_x = (x + w / 2) / width
        norm_y = (y + h / 2) / height
        norm_w = w / width
        norm_h = h / height
        
        cv2.imwrite(f'yolov5/data/validation/images/frame_{frame_count}.jpg', frame)
        print('Saved frame', frame_count)
        with open(f'yolov5/data/validation/labels/frame_{frame_count}.txt', 'w') as f:
            f.write(f'0 {norm_x} {norm_y} {norm_w} {norm_h}\n')

    cv2.imshow("Tracking", frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

video.release()
cv2.destroyAllWindows()
