import cv2
import numpy as np
import torch

# Load the YOLOv5 model using torch.hub.load
model = torch.hub.load('ultralytics/yolov5', 'yolov5s')

# Set the device to 'cuda' if available, otherwise use 'cpu'
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model.to(device)

# Create a Kalman filter object
kalman = cv2.KalmanFilter(8, 4)

# Define the state matrix (x, y, w, h, dx, dy, dw, dh)
kalman.statePre = np.array([0, 0, 0, 0, 0, 0, 0, 0], dtype=np.float32)

# Define the measurement matrix (x, y, w, h)
kalman.measurementMatrix = np.array([[1, 0, 0, 0, 0, 0, 0, 0],
                                     [0, 1, 0, 0, 0, 0, 0, 0],
                                     [0, 0, 1, 0, 0, 0, 0, 0],
                                     [0, 0, 0, 1, 0, 0, 0, 0]], dtype=np.float32)

# Define the control matrix
kalman.controlMatrix = np.zeros((8, 4), np.float32)

# Define the process noise covariance matrix
kalman.processNoiseCov = 1e-4 * np.eye(8, dtype=np.float32)

# Define the measurement noise covariance matrix
kalman.measurementNoiseCov = 1e-1 * np.eye(4, dtype=np.float32)

# Initialize variables
is_first_detection = True
tracked_position = None

# Open video capture
cap = cv2.VideoCapture(0)

while cap.isOpened():
    ret, frame = cap.read()
    if not ret:
        break

    # Perform person detection with YOLOv5
    detections = model(frame)
    for detection in detections.xyxy[0]:
        if detection[4] > 0.5 and detection[5] == 0:
            bbox = detection[:4]
            measurement = np.array(bbox, dtype=np.float32)
            # an if function is added if the if_first_detection is true then the kalman filter is initialized
            # else the kalman filter is updated
            if is_first_detection:
                kalman.statePre[:4] = measurement
                is_first_detection = False
            else:
                kalman.correct(measurement)

    prediction = kalman.predict()
    predicted_box = prediction[:4]

    # Draw the predicted bounding box on the frame
    x, y, w, h = predicted_box.astype(int)
    cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

    cv2.imshow("Object Tracking", frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
