import os
import xml.etree.ElementTree as ET

dataset_path = '/home/zoro/FAB/OpenCV/opencv/PreTrained Model/train'
annotation_path = 'haar_annotation/'
if not os.path.exists(annotation_path):
    os.makedirs(annotation_path)

# Iterate through the XML annotation files
for filename in os.listdir(dataset_path):
    if filename.endswith('.xml'):
        xml_path = os.path.join(dataset_path, filename)
        tree = ET.parse(xml_path)
        root = tree.getroot()

        # Extract relevant information from the XML file
        image_path = os.path.join(dataset_path, root.find('path').text)
        objects = root.findall('.//object')

        annotation_file_path = os.path.join(annotation_path, filename.replace('.xml', '.txt'))
        with open(annotation_file_path, 'w') as f:
            for obj in objects:
                bbox = obj.find('bndbox')
                xmin = int(bbox.find('xmin').text)
                ymin = int(bbox.find('ymin').text)
                xmax = int(bbox.find('xmax').text)
                ymax = int(bbox.find('ymax').text)
                f.write(f'{xmin} {ymin} {xmax} {ymax}\n')






