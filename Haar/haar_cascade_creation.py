import cv2
import os
import numpy as np

negative_image_path = 'dataset/negative/'
positive_image_path = 'dataset/positive/'
annotation_path = 'haar_annotation/'
positive_sample_path = 'positive_samples.vec'
training_data = 'train_data.txt'
classifier_output_file = 'messi_classifier.xml'


def create_positive_samples():
    with open (positive_sample_path, 'w') as f:
        for filename in os.listdir(annotation_path):
            annotation_file = os.path.join(annotation_path, filename)
            image_file = os.path.join(positive_image_path, filename.replace('.txt', '.jpg'))
            annotation_file = open(annotation_file, 'r')
            bbox = annotation_file.read()
            bbox = bbox.split(' ')
            sample = image_file + ' ' + bbox[0] + ' ' + bbox[1] + ' ' + bbox[2] + ' ' + bbox[3]
            f.write(f'{sample}\n')
            
def create_negative_samples():
    with open ('negative_samples.txt', 'w') as f:
        for filename in os.listdir(negative_image_path):
            image_path = os.path.join(negative_image_path, filename)
            f.write(f'{image_path}\n')
            

def create_training_data():
    with open (training_data, 'w') as f:
        for filename in os.listdir(positive_image_path):
            filename = os.path.join(positive_image_path, filename)
            f.write(f'{filename} 1\n')
        for filename in os.listdir(negative_image_path):
            filename = os.path.join(negative_image_path, filename)
            f.write(f'{filename} -1\n')


def feature_extraction(image_path):
    image = cv2.imread(image_path)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.equalizeHist(gray)
    gray = cv2.resize(gray, (24, 24))
    return gray.flatten()


def training_model():
    training_images = []
    labels = []
    with open (training_data, 'r') as f:
        for line in f:
            line = line.split(' ')
            image_path = line[0]
            print(image_path)
            feature = feature_extraction(image_path)
            training_images.append(feature)
            labels.append(int(line[1]))
    
    training_samples = np.array(training_images, dtype=np.float32)
    training_labels = np.array(labels, dtype=np.int32)

    svm = cv2.ml.SVM_create()
    svm.setType(cv2.ml.SVM_C_SVC)
    svm.setKernel(cv2.ml.SVM_LINEAR)
    svm.train(training_samples, cv2.ml.ROW_SAMPLE, training_labels)
    svm.save(classifier_output_file)
    
    # classifier = cv2.ml.Boost_create()
    # train_data = cv2.ml.TrainData_create(training_samples, cv2.ml.ROW_SAMPLE, training_labels)
    # classifier.train(train_data)

    # classifier.save(classifier_output_file)
           
create_positive_samples()
create_negative_samples()
create_training_data()
training_model()
# # Create positive samples using the opencv_createsamples tool
# command = f'createsamples -info positive.txt -vec samples.vec -w 24 -h 24'
# os.system(command)



